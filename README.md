
Pragmatic debian packaging for Exchange EWS Provider for Thunderbird
====================================================================


See also
--------

https://github.com/ExchangeCalendar/exchangecalendar/releases


Building
--------

    $ dpkg-buildpackage


Obtaining a new upstream version
--------------------------------

    $ git remote add upstream https://github.com/ExchangeCalendar/exchangecalendar.git
    $ git fetch upstream
    $ git checkout debian
    $ git merge upstream/master
